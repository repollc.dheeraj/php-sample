<?php

require_once 'App/Controllers/AuthController.php';

    $controller  =  new AuthController;


    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        $apple = $controller->register($_POST);

          if(isset($apple['errors'])){
            $errors = $apple['errors'];
          }
    }

 ?>





<html>

  <style>
  </style>
  <head>
  </style>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></editablescript>
  <script src="https://use.fontawesome.com/c92238ec5f.js"></script>

  </head>
  <body>
  <div class="container mt-4">

    <form method="post">
        <div class="row mt-3">
            <div class="col-3">
              <input class="form-control" type="text" placeholder="FirstName" name="name" >
              <?php if(isset($errors['name'])):?>
                <span class="error text-danger"><?php echo $errors['name'];  ?>
                <?php endif ?>
            </div>

            <div class="col-3">
              <input class="form-control" type="text" placeholder="LastName" name="lastname">
              <?php if(isset($errors['lastname'])):?>
                <span class="error text-danger"><?php echo $errors['lastname'];   ?>
                <?php endif ?>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-3">
              <input class="form-control" type="text" placeholder="FatherName" name="fathername">
              <?php if(isset($errors['fathername'])):?>
                <span class="error text-danger"><?php echo $errors['fathername']; ?>
                <?php endif ?>
            </div>
            <div class="col-3">
              <input class="form-control" type="text" placeholder="MotherName" name="mothername">
              <?php if(isset($errors['mothername'])):?>
                <span class="error text-danger"><?php echo $errors['mothername']; ?>
                <?php endif ?>
            </div>
        </div>

        <div class="row mt-3">
          <div class="col-6">
            <input class="form-control" type="email" placeholder="Email ID"  name="email">
            <?php if(isset($errors['email'])):?>
              <span class="error text-danger"><?php echo $errors['email']; ?>
              <?php endif ?>
          </div>
       </div>

       <div class="row mt-3">
         <div class="col-6">
           <input class="form-control" type="text" placeholder="Address"  name="address">
           <?php if(isset($errors['address'])):?>
             <span class="error text-danger"><?php echo $errors['address']; ?>
             <?php endif ?>
         </div>
         </div>
      <div class="row mt-3">
         <div class="col-6">
          <button type="submit" class="btn btn-primary btn-block">Submit</button>
        </div>
      </div>
    </form>
  </div>
 </body>
</html>
